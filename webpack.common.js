const path = require("path");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const { dependencies } = require("./package.json");
const { ModuleFederationPlugin } = require("webpack").container;

module.exports = {
    entry: "./src/index",
    output: {
        publicPath: "auto",
    },
    resolve: {
        extensions: [".tsx", ".ts", ".jsx", ".js", ".json"],
    },
    devServer: {
        port: 3001,
        historyApiFallback: true,
        static: {
            directory: path.join(__dirname, "dist"),
        },
    },
    module: {
        rules: [
            {
                test: /\.m?js/,
                type: "javascript/auto",
                resolve: {
                    fullySpecified: false,
                },
            },
            {
                test: /\.(css|s[ac]ss)$/i,
                use: ["style-loader", "css-loader", "postcss-loader"],
            },
            {
                test: /\.(ts|tsx|js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                },
            },
        ],
    },
    plugins: [
        new ModuleFederationPlugin({
            name: "coaching_stack_test",
            filename: "remoteEntry.js",
            remotes: {
                coaching_stack_test: "coaching_stack_test@http://localhost:3001/remoteEntry.js",
                sso_ui: "sso_ui@http://localhost:3000/remoteEntry.js",
            },
            exposes: {
                "./CoachingStack": "./src/CoachingStack",
            },
            shared: {
                react: {
                    eager: true,
                    singleton: true,
                    requiredVersion: dependencies.react,
                },
                "react-dom": {
                    eager: true,
                    singleton: true,
                    requiredVersion: dependencies["react-dom"],
                },
            },
        }),
        new HTMLWebpackPlugin({
            template: path.resolve("public/index.html"),
        }),
    ],
};
