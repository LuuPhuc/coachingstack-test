import React from "react";
import "./App.css";

const RemoteHeader = React.lazy(() => import("sso_ui/Header"));

function App() {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    return (
        <React.Suspense fallback='Loading Button'>
            <div className='App'>
                <RemoteHeader open={open} handleOpen={handleOpen} handleClose={handleClose} />
                <div>Coaching stack test</div>
            </div>
        </React.Suspense>
    );
}

export default App;
