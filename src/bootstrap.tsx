import React from "react";
import ReactDOM, { Root } from "react-dom/client";
import App from "./CoachingStack";
import "./index.css";

const rootElement: any = document.getElementById("root");

const root: Root = ReactDOM.createRoot(rootElement);

root.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>
);
