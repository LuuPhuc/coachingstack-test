/// <reference types="react" />

declare module "sso_ui/Header" {
    const Header: React.ComponentElement;

    export default Header;
}
